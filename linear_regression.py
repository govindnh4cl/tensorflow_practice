import numpy as np
import tensorflow as tf

np.random.seed(1337)

def get_data():
    x_data = np.random.randn(2000, 3)
    w_real = [0.3, 0.5, 0.1]
    b_real = -0.2

    noise = np.random.randn(1, 2000) * 0.1
    y_data = np.matmul(w_real, x_data.T) + b_real + noise

    return x_data, y_data, w_real, b_real

def main():
    num_steps = 10
    g = tf.Graph()
    wb_ = list()

    x_data, y_data, w_real, b_real = get_data()    

    with g.as_default():
        x = tf.placeholder(dtype=tf.float32, shape=(None, 3))
        y_true = tf.placeholder(dtype=tf.float32, shape=(None))

        w = tf.Variable([[0, 0, 0]], dtype=tf.float32, name='weights')
        b = tf.Variable(0, dtype=tf.float32, name='bias')
        y_pred = tf.matmul(w, tf.transpose(x))  # shape 1xNone

        loss = tf.reduce_mean(tf.square(y_true - y_pred))

        learning_rate = 0.5
        optimizer = tf.train.GradientDescentOptimizer(learning_rate)
        train = optimizer.minimize(loss)

        init = tf.global_variables_initializer()
        
        with tf.Session() as sess:
            sess.run(init)
            for step in range(num_steps):
                sess.run(train, {x: x_data, y_true: y_data})

                if step % 5 == 0:
                    w_, b_ = sess.run([w, b])
                    print(step, 'W: {:} B: {:}'.format(w_, b_))
                    wb_.append([w_, b_])

            print(num_steps, 'W: {:} B: {:}'.format(w_, b_))


if __name__ == '__main__':
    main()
